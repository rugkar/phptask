# Completed task for the Junior PHP developer role at payments.




## All the steps required to get the application running:

1. To run the project you should have **XAMP server**, **PHP**, and **Composer** installed on your device.

2. Pull or download the project from Bitbucket.

3. Run the XAMPP server on your system, open PHPMYADMIN, create a database named 'Uzduotis' and import the dump file *'Uzduotis.sql'*. It will create the table 'cars' with demo data.

4. Rename `.env.example` file to `.env` inside the project root and change the database information:

		DB_DATABASE=Uzduotis
		DB_USERNAME=root
		DB_PASSWORD=

5. Open the console and `cd` to the directory of the project.

6. Run `composer install`.

7. Run `php artisan key:generate`.

8. Run `php artisan serve`.

9. Copy the development server address to your browser.
	
	
	
	
## How to use the project:

On the **main page**, you will see the list of all the cars that are in the database.

If you wish to **filter** the cars list by price, you can do it by filling the form in the upper left corner below the header. Just enter the minimum and maximum prices by which you want to filter by and click 'Filter'. After that you will be shown a list of all the cars between your specified price frames. 

If you wish to **search** for a specific car in the list by brand or model, you can do it by filling the form in the middle top below the header. Just write your wanted model, brand or both and the results will be shown to you after you click the 'Search' button. 

If you wish to **add a new car**, you can do it by clicking the 'Add new car' button at the  upper right corner below the header. You will be redirected to a form. After you fill the form and add the image of the car, you can click submit and if the form is filled correctly, the new car will be added to the database. 

If you select one car as you click on it, you will be able to see more information about it. In each individual **car view page** you will be able to see these details: *car image, brand, model, price, fuel type, fuel type capacity, maximum speed*, as well as some optional details if there are any like *power* and *description*. 
At the bottom of the car view page there are three buttons: **'Edit'**, **'Update price'**, **'Delete'**.
	
The **'Edit'** button: if you click this button, you will be able to edit and update all details about the particular car except the image of the car. When you fill in the form with the details you are happy with you can click the 'submit' button and it will update the cars information in the database. 

The  **'Update price'** button: if you click this button, you will be redirected to the page where you will be able to edit only the cars price. After you change the price just click 'submit' and the changes will be saved in the database. 

The **'Delete'** button: after you click this button, you will see an alert, that will ask you if you are sure that you wish to delete this car. If you click 'OK', the car will be deleted from the database. If you click 'cancel' nothing will happen. 






