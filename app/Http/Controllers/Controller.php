<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
use App\Models\Car; 
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function showCars(){
        $cars = DB::table('cars')->get();
        return view('showCars',['cars'=>$cars]);
    }

    public function showCar($ID ){
        $car = Car::find($ID);
        return view('viewCar',['car'=>$car]);
    }

    public function addCar(Request $req ){
        $validated = $req->validate([
            'Image' => 'required|image',
            'Brand' => 'required',
            'Model' => 'required',
            'Fuel_Type' => 'required',
            'Fuel_Tank_Cap' => 'required',
            'Max_Speed' => 'required',
            'Price' => 'required'
        ]);
       // print_r($req->input());
        $car = new Car;
        $car->Image = $req->file('Image')->get();
        $car->Brand = $req->Brand;
        $car->Model = $req->Model;
        $car->Fuel_Type = $req->Fuel_Type;
        $car->Fuel_Tank_Cap = $req->Fuel_Tank_Cap;
        $car->Max_Speed = $req->Max_Speed;
        $car->Price = $req->Price;
        $car->Power = $req->Power;
        $car->Description = $req->Description;
        //echo $req->Image;
        $car->save();
        return redirect('/');

    }

    public function deleteCar($ID){
        Car::where('ID', '=', $ID)->delete();
        return redirect('/');
    }

    public function editCar($ID ){
        $car = Car::find($ID);
        return view('editCar',['car'=>$car]);
    }

    public function updateCar(Request $req){
        $validated = $req->validate([
            'Brand' => 'required',
            'Model' => 'required',
            'Fuel_Type' => 'required',
            'Fuel_Tank_Cap' => 'required',
            'Max_Speed' => 'required',
            'Price' => 'required'
        ]);

        DB::table('cars')
            ->where('id', $req->ID)
            ->update(['Brand' => $req->Brand,
                    'Model' => $req->Model,
                    'Fuel_Type' => $req->Fuel_Type,
                    'Fuel_Tank_Cap' => $req->Fuel_Tank_Cap,
                    'Max_Speed' => $req->Max_Speed,
                    'Price' => $req->Price,
                    'Power' => $req->Power,
                    'Description' => $req->Description,]);
        $car = Car::find($req->ID);
        return view('viewCar',['car'=>$car]);
    }

    public function updateCarPrice($ID ){
        $car = Car::find($ID);
        return view('updatePrice',['car'=>$car]);
    }

    public function editPrice(Request $req){
        $validated = $req->validate([
            'Price' => 'required'
        ]);

        DB::table('cars')
            ->where('id', $req->ID)
            ->update(['Price' => $req->Price,]);
        $car = Car::find($req->ID);
        return view('viewCar',['car'=>$car]);
    }

    public function filterCars(Request $req){
        $req->priceFrom != null ? $from=$req->priceFrom : $from=0;
        $req->priceTo != null ? $to=$req->priceTo : $to=PHP_INT_MAX;
        $cars = DB::table('cars')->where('Price', '>=', $from)->where('Price', '<=', $to)->orderBy('Price')->get();
        return view('showCars',['cars'=>$cars]);
    }

    public function searchCars(Request $req){
        $req->search != null ? $search=$req->search : $search='';
        $cars = DB::table('cars')->where(DB::raw("CONCAT(Brand,' ' ,Model)"), 'LIKE', "%$search%")->orderBy('Brand')->get();
        return view('showCars',['cars'=>$cars]);

    }
}
