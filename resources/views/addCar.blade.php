<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cars</title>

        <!-- Fonts -->
       

        <!-- Styles -->
        <link rel="stylesheet"  href="../css/app.css" />
    </head>
    <body>

    <div class="navBar"> 
       <h1>ADD CAR</h1>
    </div>
    <a style='color:red' href='/'>| Go to list |</a>
    @if ($errors->any())
    <div class="alert alert-danger">
        <h2>Please check the form input. It must follow these rules:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        @endif
       <form class='addCar' action='submit' method='POST' enctype="multipart/form-data">
       @csrf
        Photo:<input type="file" name="Image">
        <br>
        Brand:<input type='text' name='Brand'><br>
        Model: <input type='text' name='Model'><br>
        Fuel Type: <input type='text' name='Fuel_Type'><br>
        Fuel Tank Capacity: <input type='number' name='Fuel_Tank_Cap'> <br>
        Maximum speed: <input type='number' name='Max_Speed'> <br>
        Price: <input type='number' name='Price'> <br>
        Power: <input type='number' name='Power'> <br>
        Description: <textarea cols="40" rows="5" placeholder='Write the cars description here' name='Description'></textarea><br>
        
        <br>       
        <input type='submit'>
       </form>
    </body>
</html>