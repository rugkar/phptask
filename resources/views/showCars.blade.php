<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cars</title>

        <!-- Fonts -->
       

        <!-- Styles -->
        <link rel="stylesheet"  href="../css/app.css" />
    </head>
    <body>
       <div class="navBar"> 
       <h1>ALL CARS</h1>
        </div>
        <div class="filter">
            <form action="filter" method="get">
            Price from: <input type="number" name="priceFrom">€
            to: <input type="number" name="priceTo">€
                <input type="submit" value="Filter">
            </form>
            <form action="search" method="get">
                Searh: <input type="text" name="search">
                <input type="submit" value="Search">
            </form>
            <a style='color:red' href='/add'>| Add new car |</a>
            </div>
            <div class="carListBox">
           
            @if(count($cars) == 0)
         <div class="alert alert-danger">
             <h1>NO CARS FOUND</h1>
         </div>
        @else
            @foreach($cars as $car)
            <a href='/view{{ $car->ID }}'>
            <div class="carBox">
                    <p class="carModel">{{$car->Brand}} {{$car->Model}}</p>
                    <img src="data:image/jpeg;base64,<?php echo base64_encode( $car->Image ); ?>" class="carPhoto" />
                    <p>{{$car->Price}}€ </p>
                </div>
                </a> 
            @endforeach
            @endif

            </div>

       <div>
           
       </div>
    </body>
</html>
