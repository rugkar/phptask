<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cars</title>

        <!-- Fonts -->
       

        <!-- Styles -->
        <link rel="stylesheet"  href="../css/app.css" />
    </head>
    <body>
       <div class="navBar"> 
            <h1>{{$car->Brand}} {{$car->Model}}</h1>
       </div>
        <a style='color:red' href='/'>| Go to list |</a>
       <div class="carViewBox">
                    <img src="data:image/jpeg;base64,<?php echo base64_encode( $car->Image ); ?>" class="imageView" />
                    <br>
                    @if ($car->Description != null)
                    <h3>{{$car->Description}}</h3>
                    @endif

                    <table>

                    <tr>
                 <td class=>Price</th>
                 <td class="">{{$car->Price}} €</th>
                </tr>
                    <tr>
                 <td class="">Fuel Type</td>
                <td class="">{{$car->Fuel_Type}}</td>
                    </tr>
                <tr>
                <td class="">Fuel Tank Capacity</td>
                <td class="">{{$car->Fuel_Tank_Cap}} l</td>
                     </tr>
                    
                <tr>
                 <td class="">Maximum Speed</td>
                 <td class="">{{$car->Max_Speed}} km/h</td>
                     </tr>
                   
                    @if ($car->Power != null)
                     <tr>
                <td class="">Power</td>
                <td class="">{{$car->Power}} kW</td>
                 </tr>
                 @endif
                 <tr>
                 <td class=""></td>
                 <td class=""></td>
                </tr>
             </table>
                    <div class="editDeleteBtns">
                    <a style='color:red' href='/edit{{$car->ID}}'> | Edit | </a> <br>
                    <a style='color:red' href='/updatePrice{{$car->ID}}'> | Update price | </a> <br>
                    <a style='color:red' onclick="return confirm('Are you sure you want to delete this car?')" href='/delete{{$car->ID}}'> | Delete | </a>
                    </div>

        </div>
    </body>
</html>
