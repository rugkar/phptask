<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cars</title>

        <!-- Fonts -->
       

        <!-- Styles -->
        <link rel="stylesheet"  href="../css/app.css" />
    </head>
    <body>

    <div class="navBar"> 
       <h1>UPDATE {{$car->Brand}} {{$car->Model}} PRICE</h1>
    </div>
    <a style='color:red' href='/'>| Go to list |</a> <br>
    @if ($errors->any())
    <div class="alert alert-danger">
        <h2>Please check the form input. It must follow these rules:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        @endif
        <div class="carViewBox">

        <img src="data:image/jpeg;base64,<?php echo base64_encode( $car->Image ); ?>" class="imageView" />
        <br>
        
       <form class='' action='updatePrice' method='POST' enctype="multipart/form-data">
       @csrf
       <input type='hidden' name="ID" value="{{$car->ID}}">
        New price: <input type='number' name='Price' value="{{$car->Price}}">€ <br>
        <br>       
        <input type='submit' >
       </form>
       </div>
    </body>
</html>