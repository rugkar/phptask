<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cars</title>

        <!-- Fonts -->
       

        <!-- Styles -->
        <link rel="stylesheet"  href="../css/app.css" />
    </head>
    <body>

    <div class="navBar"> 
       <h1>EDIT {{$car->Brand}} {{$car->Model}}</h1>
    </div>
    <a style='color:red' href='/'>| Go to list |</a>
    @if ($errors->any())
    <div class="alert alert-danger">
        <h2>Please check the form input. It must follow these rules:</h2>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        @endif
       <form class='addCar' action='edit' method='POST' enctype="multipart/form-data">
       @csrf
       <input type='hidden' name="ID" value="{{$car->ID}}">
        Brand:<input type='text' name='Brand' value="{{$car->Brand}}"><br>
        Model: <input type='text' name='Model' value="{{$car->Model}}"><br>
        Fuel Type: <input type='text' name='Fuel_Type' value="{{$car->Fuel_Type}}"><br>
        Fuel Tank Capacity: <input type='number' name='Fuel_Tank_Cap' value="{{$car->Fuel_Tank_Cap}}"> <br>
        Maximum speed: <input type='number' name='Max_Speed' value="{{$car->Max_Speed}}"> <br>
        Price: <input type='number' name='Price' value="{{$car->Price}}"> <br>
        Power: <input type='number' name='Power' value="{{$car->Power}}"> <br>
        Description: <textarea cols="40" rows="5" placeholder='Write the cars description here' name='Description' >{{$car->Description}}</textarea><br>
        
        <br>       
        <input type='submit' >
       </form>
    </body>
</html>