<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [Controller::class, 'showCars']);
Route::get('/view{ID}',[Controller::class, 'showCar']);
Route::view('/add','addCar');
Route::post('/submit',[Controller::class, 'addCar']);
Route::get('/delete{ID}',[Controller::class, 'deleteCar']);
Route::get('/edit{ID}',[Controller::class, 'editCar']);
Route::post('edit',[Controller::class, 'updateCar']);
Route::get('/updatePrice{ID}',[Controller::class, 'updateCarPrice']);
Route::post('updatePrice',[Controller::class, 'editPrice']);
Route::get('filter',[Controller::class, 'filterCars']);
Route::get('search',[Controller::class, 'searchCars']);




